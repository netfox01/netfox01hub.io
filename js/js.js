if (typeof google !== 'undefined') {
    // When the window has finished loading create our google map below
    google.maps.event.addDomListener(window, 'load', init);

    function init() {
        // Basic options for a simple Google Map
        // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
        var mapOptions = {
            // How zoomed in you want the map to start at (always required)
            zoom: 15,

            // The latitude and longitude to center the map (always required)
            center: new google.maps.LatLng(33.6009837, -7.6214938), // New York

            // How you would like to style the map.
            // This is where you would paste any style found on Snazzy Maps.
            styles: [{"featureType":"landscape","stylers":[{"saturation":-100},{"lightness":65},{"visibility":"on"}]},{"featureType":"poi","stylers":[{"saturation":-100},{"lightness":51},{"visibility":"simplified"}]},{"featureType":"road.highway","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"road.arterial","stylers":[{"saturation":-100},{"lightness":30},{"visibility":"on"}]},{"featureType":"road.local","stylers":[{"saturation":-100},{"lightness":40},{"visibility":"on"}]},{"featureType":"transit","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"administrative.province","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"labels","stylers":[{"visibility":"on"},{"lightness":-25},{"saturation":-100}]},{"featureType":"water","elementType":"geometry","stylers":[{"hue":"#ffff00"},{"lightness":-25},{"saturation":-97}]}]
        };

        // Get the HTML DOM element that will contain your map
        // We are using a div with id="map" seen below in the <body>
        var mapElement = document.getElementById('map');

        // Create the Google Map using our element and options defined above
        var map = new google.maps.Map(mapElement, mapOptions);

        // Let's also add a marker while we're at it
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(33.598776, -7.620957),
            map: map,
            title: 'Snazzy!'
        });
    }
};

jQuery(document).ready(function($) {
    if (typeof WOW !== 'undefined') {
        $('.container').each(function (el, index) {
            $(this).addClass('wow fadeIn animated').attr('data-wow-delay',"0.5s");
        })
    };
    new WOW().init();
    var today = new Date();
    var thiyear = today.getFullYear();
    var mybirthyear = 1991;
    myage = document.getElementsByClassName("myage");
    for (var i = 0; i < myage.length; i++) {
        myage[i].innerText  = thiyear - mybirthyear;
    };

/*    $('button[type="submit"]').click(function(event) {
        event.preventDefault();
        lang = $('body').hasClass('lang-fr') ? "fr" : "en";
        $.ajax({
            url: 'http://getsimpleform.com/messages/ajax?form_api_token=b0ff6cf30c2bd4f8a42baa605ceebbf4',
            // url: 'localhost/projets-php/mailme/index.php?msglang='.lang,
            type: 'POST',
            data: {
                firs_name : $('[name="firs_name"]').val(),
                last_name : $('[name="last_name"]').val(),
                email_address : $('[name="email_address"]').val(),
                message : $('[name="message"]').val(),
                subject : $('[name="subject"]').val()
            },
        })
        .done(function() {
            if (lang == "fr") {
                message = "Merci de m'avoir contacter,  je vous répondrai dans les plus brefs délais.";
                alert(message);
            } else{
                message = "Thank you for contacting me, i'll get back to you as soon as possible.";
                alert(message);
            };
        })
        .fail(function(result) {
            if (lang == "fr") {
                message = "Une erreur s'est produite lors de l'envoi du message, merci de me contacter par mail <strong>eissa.soubhi@gmail.com</strong> ou par téléphone <strong>+212 663 317 740</strong>.";
                alert(message);
            } else{
                message = "An error occurred while sending the message, please contact me by e-mail <strong>eissa.soubhi@gmail.com</strong> or phone <strong>+212 663 317 740</strong>.";
                alert(message);
            };
        });
    });*/
    hostname = location.origin;
    thankyou = hostname + "/" + ($('body').hasClass('lang-fr') ? "fr/merci.html" : "en/thank-you.html");
    $('#contact-form').attr('action', 'https://getsimpleform.com/messages?form_api_token=b0ff6cf30c2bd4f8a42baa605ceebbf4').append('<input type="hidden" name="redirect_to" value="'+thankyou+'" />');
})